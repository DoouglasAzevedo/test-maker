import React from 'react'
import { withRouter } from 'react-router-dom'

class Questions extends React.Component {

    constructor(props) {
        super(props)
        
        this.state = {
            testId: props.match.params.test
        } 
    }

    render() {
        return (
            <div>
                Título da questão Título da questão Título da questão <br />
                <ul>
                    <li>Resposta 1</li>
                    <li>Resposta 2</li>
                    <li>Resposta 3</li>
                    <li>Resposta 4</li>
                </ul>

                <input type="button" value="Nova Questão" />
            </div>
        )
    }

}

export default withRouter(Questions)